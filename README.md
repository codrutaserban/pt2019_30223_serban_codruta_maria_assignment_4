# Restaurant #

Aceasta aplicatie ii pune la dispozitie utilizatorului prin intermediul interfetei grafice
multitudine de functionalitati. Printre acestea se numara adaugarea unei noi optiuni in meniul
restaurantului, editarea uneia deja existente, stergerea unui anumit preparat din meniu precum si
vizualizarea tuturor elementelor din acesta. Toate aceste optiuni mentionate pot stau la dispozitia
administratorului. Pentru simplificarea problemei consideram ca avem doar un administrator, un
ospatar si un chef la bucatarie. Operatiile care stau la dispozitia ospatarului sunt urmatoarele:
crearea unei noi comenzi , vizualizarea tuturor comenzilor deja create, sub forma unui tabel,
precum si crearea unei facturi. Cheful este doar informat atunci cand o noua comanda este creata
si pentru evidentierea acestei trasaturi a aplicatiei se deschide pe ecran o casuta de text care ne 
4
informeaza referitor la acest fapt. De asemenea ceful poate fi accesat din pagina d epornire a
aplicatiei si acolo suntem informati de numarul de comenzi pe care trebuie sa le onoreze acesta.
