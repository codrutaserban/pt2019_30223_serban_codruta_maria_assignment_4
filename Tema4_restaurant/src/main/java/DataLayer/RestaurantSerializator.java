package DataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class RestaurantSerializator implements java.io.Serializable{
	String fisier="Restaurant.ser";
	public void serializare(Restaurant r) {
		try {
			FileOutputStream fisierIesire = new FileOutputStream(fisier);
			ObjectOutputStream out = new ObjectOutputStream(fisierIesire);
						out.writeObject(r.getMeniu());
			out.close();
			fisierIesire.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Restaurant deserializare(Restaurant r) {
		try {
			FileInputStream fisierIntrare = new FileInputStream(fisier);
			ObjectInputStream in = new ObjectInputStream(fisierIntrare);
			
			r.setMeniu((ArrayList<MenuItem>) in.readObject());
			in.close();
			fisierIntrare.close();
		}
		catch(Exception e){
			e.printStackTrace();
			}
		return r;
	}
}
