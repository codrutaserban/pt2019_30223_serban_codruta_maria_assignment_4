package PT2019.demo.Tema4_restaurant;

import BusinessLayer.Restaurant;
import DataLayer.RestaurantSerializator;
import PresentationLayer.PaginaPornire;

public class Main {

	public static void main(String[] args)  {
		
		Restaurant r= new Restaurant();
		r.init();
		RestaurantSerializator s= new RestaurantSerializator();
		s.deserializare(r);		
		PaginaPornire p= new PaginaPornire(r);
		s.serializare(r);
	}

}
