package BusinessLayer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.text.html.HTMLDocument.Iterator;

public class Restaurant extends Observable implements IRestaurantProcessing  {
	Map<Order, Collection<MenuItem>> comanda = new HashMap<Order, Collection<MenuItem>>();
	Collection<MenuItem>  produsComanda= new ArrayList<MenuItem>();
	ArrayList<MenuItem> meniu = new ArrayList<MenuItem>();
	ArrayList<Order> o=new ArrayList<Order>();
	
	public void init() {
		BaseProduct p=new BaseProduct(7,"Supa");
	  
	    BaseProduct p1=new BaseProduct(12,"Gratar");
	  
	    BaseProduct p2=new BaseProduct(5,"Cartofi prajiti");
	
	    Order o1= new Order(1,"20.05.2019",5);
	    Order o2= new Order(2,"15.05.2019",4);
	    o.add(o1);
	    o.add(o2);
	    meniu.add(p);
	    meniu.add(p1);
	    meniu.add(p1);
	    produsComanda.add(p);
	    produsComanda.add(p1);
	    produsComanda.add(p2);
	    comanda.put(o1,produsComanda);
	    this.computePrice(o1);
	    this.computePrice(o2);
	    
	}
	
	public void Restaurant(ArrayList<MenuItem> r) {
		this.meniu = r;
	}
	/**
	 * @pre p.nume!=" "
	 * @pre p.pret>0
	 * @post adaugare element intr-o comanda
	 */
	public void adaugareProdusComanda(MenuItem p) {
		assert p.computePrice()>0 && (!p.getNume().equals("")) : dateIntrareInvalide();
		this.produsComanda.add(p);
	}
	
	/**
	 * @pre p.nume!=" "
	 * @pre p.pret>0
	 * @post adaugare element in meniu
	 */
	public void adaugareProdusMeniu(MenuItem p) {
		assert p.computePrice()>0 && (!p.getNume().equals("")) : dateIntrareInvalide();
		this.meniu.add(p);
		System.out.println("e bine");
	}
	
	/**
	 * @pre o.nume!=" "
	 * @pre o.masa!=0
	 * @pre o.id!=0
	 * @pre !c.isEmpty()
	 * @pre o.pret>0
	 * @pre o.data!=""
	 * @post crearea unei comenzi noi
	 */
	public void adaugareComanda(Order o, Collection<MenuItem> c) {
		assert o.getId()!=0 && o.getMasa()!=0 && !c.isEmpty() && !o.getData().equals("") && o.getPret()>0 :dateIntrareInvalide();
		assert verificareHashMap(o)==false :idInvalid();	
		comanda.put(o,c);
		}
	
	/**
	 * @pre o.nume!=" "
	 * @pre o.masa!=0
	 * @pre o.id!=0
	 * @pre !c.isEmpty()
	 * @pre o.pret>0
	 * @pre o.data!=""
	 * @post o.pret>0
	 */
	public int computePrice(Order o) {
		int p=0;
		assert o.getId()!=0 && o.getMasa()!=0 && !o.getData().equals("") && o.getPret()>0 :dateIntrareInvalide();		
		Set<Order> aux= this.getComanda().keySet();
		Collection<MenuItem> values=new ArrayList<MenuItem>();
		for(Order j:aux)
		{
			if(j.equals(o)) {
				values=comanda.get(o);
				for(MenuItem x:values) {
					p=p+x.computePrice();
			}
		}
		
	}
		assert p>0 : invalid();
		o.setPret(p);
		return p;
}
	
	public int dateIntrareInvalide() {
		JOptionPane.showMessageDialog(null, "Datele de intrare introduse sunt invalide!");
		return 0;
	}
	public int invalid() {
		JOptionPane.showMessageDialog(null, "Eror");
		return 0;
	}
	
	public int idInvalid() {
		JOptionPane.showMessageDialog(null, "Comanda exista deja. Id-ul este gresit!");
	return 0;
	}
	
	public Map<Order, Collection<MenuItem>> getComanda() {
		return comanda;
	}
	//well formed
	public boolean verificareHashMap (Order o) {
		return comanda.containsKey(o);
	}
	
	public ArrayList<MenuItem> getMeniu() {
		return meniu;
	}

	public void setMeniu(ArrayList<MenuItem> meniu) {
		this.meniu = meniu;
	}
	
	/**
	 * @pre m.getNume().equals(name)==true
	 * @post ok=1
	 */
	public void stegereProdusMeniu(String name) {
		int ok=0;
		for(MenuItem m:meniu) {
			if(m.getNume().equals(name)) {
				meniu.remove(m);
				ok=1;
				break;
			}
			System.out.println("e bine");
		}
		assert ok>0 : invalid();
	}
	
	/**
	 * @post ok=1
	 * @pre i.equals(m)==true
	 */
	public void editProdusMeniu(MenuItem m, String camp, String nouaVal) {
		int ok=0;
		for(MenuItem i:meniu) {
			if(i.equals(m)) {
				if(camp.equals("Nume")) {
					m.setNume(nouaVal);
				}
				if(camp.equals("Pret")) {
					m.setPret(Integer.parseInt(nouaVal));
				}
				ok=1;
			}
		}
		assert ok>0 : invalid();	
	}
	/**
	 * @throws IOException 
	 * @pre o.nume!=" "
	 * @pre o.masa!=0
	 * @pre o.id!=0
	 * @pre !c.isEmpty()
	 * @pre o.pret>0
	 * @pre o.data!=""
	 * @post @nochange
	 */
	public void generareFactura(Order o)  throws IOException
	{
		
		 File f= new File(String.format("Factura%d.txt",o.getId()));
		 BufferedWriter writer = null;
		 try {writer = new BufferedWriter(new FileWriter(f));}
		 catch(Exception e){JOptionPane.showMessageDialog(null, "Eroare la crearea fisierului!");return;}
		 String titlu="             Factura\n\n",produs="Produse: \n",total="Total:      ";
		 String produse= new String();
		 int pret=0;
			assert o.getId()!=0 && o.getMasa()!=0 && !o.getData().equals("") && o.getPret()>0 :dateIntrareInvalide();
			Set<Order> aux= this.getComanda().keySet();
			Collection<MenuItem> values=new ArrayList<MenuItem>();
			for(Order j:aux)
			{
				if(j.getId()==o.getId()) {
					values=comanda.get(o);
					for(MenuItem k:values) {
						produse=produse+" "+ k.getNume()+":  "+k.computePrice()+" lei\n";
						pret=pret+k.computePrice();}
				}
			}
			
		 writer.write(titlu+produs+produse+total+Integer.toString(pret));	
		 try {writer.close();} 
			catch (Exception e) {}
	}
	public ArrayList<Order> getO() {
		return o;
	}
	public void setO(ArrayList<Order> o) {
		this.o = o;
	}
	
	
}
