package BusinessLayer;

public class Order  implements java.io.Serializable {
	private int id;
	private String data;
	private int pret;
	private int masa;
	
	 public Order() {
		
	}
	
	 public Order(int id, String data, int masa){
	this.id=id;
	this.data=data;
	this.masa=masa;
	 }
	 
	 public int hashCode() {
			int hash = 0;
			hash = (this.getMasa() + 19 * id + 29 * id * id) % 379;
			return hash;
		}
	 
	
	 public boolean equals(Order i) {
		 if(this.id== i.id && this.data.equals(i.data) && this.pret==i.pret && this.masa==i.masa )
			 return true;
		 else return false;
	 }
	 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public int getMasa() {
		return masa;
	}

	public void setMasa(int masa) {
		this.masa = masa;
	}
	 
}
