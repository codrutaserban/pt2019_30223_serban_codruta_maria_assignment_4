package BusinessLayer;

public abstract class MenuItem  implements java.io.Serializable {
	 public abstract int computePrice() ;
	 public abstract void setPret(int pret);
	 public abstract String getNume();
	 public abstract void setNume(String nume);
	 public abstract String getPret();
	 MenuItem(){
		 
	 }
}
