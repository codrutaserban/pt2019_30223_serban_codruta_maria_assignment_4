package BusinessLayer;

public class BaseProduct extends MenuItem  implements java.io.Serializable {
	private int pret;
	private String nume;
	
	public BaseProduct(int p, String n)
	{
		pret=p;
		nume=n;
	}
	
	public int computePrice() {
		return pret;
	}

	public String getPret() {
		return Integer.toString(pret);
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}
	
}
