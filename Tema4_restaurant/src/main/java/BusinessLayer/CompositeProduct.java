package BusinessLayer;

import java.util.*;

public class CompositeProduct extends MenuItem  implements java.io.Serializable {
	private int pret;
	private ArrayList<MenuItem> list= new ArrayList<MenuItem>();
	private String nume;
	
	CompositeProduct(String n){
	nume=n;
	}
	
	public int computePrice() {
		
		int p = 0;
		for (MenuItem i : list) {
			p += i.computePrice();
		}
		pret =p;
		return p;
	}
	public void adaugare(MenuItem item) {
		list.add(item);
	}
	
	public MenuItem getElement(int i)
	{
		return list.get(i);
	}

	public String getPret() {
		return Integer.toString(pret);
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public ArrayList<MenuItem> getList() {
		return list;
	}

	public void setList(ArrayList<MenuItem> list) {
		this.list = list;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}
	
	
}
