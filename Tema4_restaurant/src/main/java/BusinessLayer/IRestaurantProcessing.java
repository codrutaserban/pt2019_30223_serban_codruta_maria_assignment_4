package BusinessLayer;

import java.io.IOException;
import java.util.Collection;

public interface IRestaurantProcessing {
	public void adaugareComanda(Order o, Collection<MenuItem> c);
	public void adaugareProdusMeniu(MenuItem p);
	public void stegereProdusMeniu(String name);
	public void editProdusMeniu(MenuItem m, String camp, String nouaVal) ;
	public void generareFactura(Order o) throws IOException;
	public int computePrice(Order o);
	
	
	
	
}
