package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.text.html.HTMLDocument.Iterator;

import BusinessLayer.Restaurant;

public class InterfataChelner {
	private JFrame frame=new JFrame("Comanda");
	private JButton viz= new JButton("Afisare comenzi");
	private JButton comanda= new JButton("Creare comanda");
	private JButton factura= new JButton("Creare factura");
	private Restaurant r=new Restaurant();
	
	InterfataChelner(){
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(450, 250);
		viz.setFont(new Font("Arial",Font.PLAIN,25));
		comanda.setFont(new Font("Arial",Font.PLAIN,25));
		factura.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    panel.add( Box.createRigidArea(new Dimension(150,25)) );
	    panel.add(comanda);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(viz);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(factura);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    
	    frame.add(panel);
		frame.setVisible(true);
		comanda.addActionListener(new opCreare());
	 	viz.addActionListener(new opViz());
	 	factura.addActionListener(new opFactura());
		
	}
	    
	private class opCreare implements ActionListener {
 		public void actionPerformed(ActionEvent e){
 				try {
 					int ok=0;
					InterfataCreareComanda produsAd=new InterfataCreareComanda(r);
					
					/*Order o=produsAd.getO();
					Iterator i=(Iterator) ((Map) comanda).entrySet().iterator();
					while(((java.util.Iterator<Entry<Order, Collection<MenuItem>>>) i).hasNext()){
					Map.Entry element=(Map.Entry) ((java.util.Iterator<Entry<Order, Collection<MenuItem>>>) i).next();
					if(element.getKey().equals(o))
					{	ok=1;
						Collection<MenuItem> m=(Collection<MenuItem>) element.getValue();
						m.add(produsAd.getI());
					}
					}
					if(ok==0) {
						
						r.getComanda().put(o,produsAd.getProduse());
					}*/
					
				} catch (Exception e1) {
					e1.printStackTrace();
				} 
 		}
	}

	public void setR(Restaurant r) {
		this.r = r;
	}
	
	
	private class opViz implements ActionListener {
 		public void actionPerformed(ActionEvent e){
 			try {
				InterfataComandaVizualizare produsViz=new InterfataComandaVizualizare(r);
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 		}
	}
	private class opFactura implements ActionListener {
 		public void actionPerformed(ActionEvent e){
 			try {
 				InterfataCreareFactura factura=new InterfataCreareFactura(r);
 								
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 		}
	}
}
