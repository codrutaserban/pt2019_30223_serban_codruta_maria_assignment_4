package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.html.HTMLDocument.Iterator;

import BusinessLayer.Order;
import BusinessLayer.Restaurant;



public class InterfataComandaVizualizare {
	private JTable tabel=new JTable();
	private DefaultTableModel model = new DefaultTableModel();
	private Object[] capDeTabel = new Object[4];
	private Object[] date = new Object[4];
	Restaurant r;//=new Restaurant();
	
	private JFrame frame = new JFrame("Vizualizare comenzi"); 
	@SuppressWarnings("unchecked")
	public InterfataComandaVizualizare (Restaurant r) throws ClassNotFoundException, SQLException{
		JScrollPane scroll;
		this.r=r;
		JPanel panel=new JPanel();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(500, 480);
		capDeTabel[0]="id";
		capDeTabel[1]="Data";
		capDeTabel[2]="Masa";
		capDeTabel[3]="Pret";
		model.setColumnIdentifiers(capDeTabel);
	
		try {
		
			for(Order j: r.getO()) {
		System.out.println("Aici");
			date[0]=j.getId();
			date[1]=j.getData();
			date[2]=j.getMasa();
			date[3]=j.getPret();
			model.addRow(date);}
		
		} catch (Exception e) {}
		
		
		tabel.setModel(model);
		tabel.setFont(new Font("Arial",Font.PLAIN,12));
		tabel.setEnabled(false);
		scroll= new JScrollPane(tabel);
		panel.add(scroll,new FlowLayout());
		frame.add(panel);
		frame.setVisible(true);
		
	}
	

}
