package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.html.HTMLDocument.Iterator;

import BusinessLayer.BaseProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

public class InterfataCreareComanda {
	private JFrame frame = new JFrame("Plasare comanda");
	private JLabel info= new JLabel("Adaugati produse:");;
	private JLabel numeProdus= new JLabel("Numele produsului: ");
	private JButton enter= new JButton("Enter");
	private JLabel cantitate= new JLabel("Pret produs: ");
	private JLabel id= new JLabel("Adaugati id-ul:");
	private JLabel masa= new JLabel("Numarul mesei: ");
	
	private JComboBox<String> tNumeProdus=new JComboBox(new String[]{});
	
	private JComboBox<String> tMasa=new JComboBox(new String[]{"1","2","3","4","5","6","7"});
	private JTextField tId= new JTextField("");
	
	InterfataChef chef= new InterfataChef();
	private Order o;
	private ArrayList<MenuItem> produse;
	private MenuItem m;
	Restaurant r;
	
	InterfataCreareComanda(Restaurant r) throws ClassNotFoundException, SQLException{
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.r=r;
		frame.setSize(550, 350);
		setCombo(r.getMeniu()) ;
		enter.setFont(new Font("Arial",Font.PLAIN,25));
		info.setFont(new Font("Arial",Font.PLAIN,25));
		numeProdus.setFont(new Font("Arial",Font.PLAIN,25));
		cantitate.setFont(new Font("Arial",Font.PLAIN,25));
		masa.setFont(new Font("Arial",Font.PLAIN,25));
		id.setFont(new Font("Arial",Font.PLAIN,25));
		
		tMasa.setFont(new Font("Arial",Font.PLAIN,25));
		tId.setFont(new Font("Arial",Font.PLAIN,25));
		tNumeProdus.setFont(new Font("Arial",Font.PLAIN,25));
		
		
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    
		
		JPanel pol= new JPanel();
	    pol.setLayout(new BoxLayout(pol, BoxLayout.X_AXIS));
	    pol.add(info);
	    
	    
	    JPanel pol2 = new JPanel();
	    pol2.setLayout(new BoxLayout(pol2, BoxLayout.X_AXIS));
	    pol2.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol2.add(numeProdus);
	    pol2.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol2.add(tNumeProdus);
	    pol2.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    /*
	    JPanel pol4= new JPanel();
	    pol4.setLayout(new BoxLayout(pol4, BoxLayout.X_AXIS));
	    pol4.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol4.add(cantitate);
	    pol4.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol4.add(tCantitate);
	    pol4.add( Box.createRigidArea(new Dimension(100,0)) );
	    */
	    JPanel pol5= new JPanel();
	    pol5.setLayout(new BoxLayout(pol5, BoxLayout.X_AXIS));
	    pol5.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol5.add(id);
	    pol5.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol5.add(tId);
	    pol5.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol6= new JPanel();
	    pol6.setLayout(new BoxLayout(pol6, BoxLayout.X_AXIS));
	    pol6.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol6.add(masa);
	    pol6.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol6.add(tMasa);
	    pol6.add( Box.createRigidArea(new Dimension(100,0)) );
	    
		JPanel pol12= new JPanel();
		pol12.setLayout(new BoxLayout(pol12, BoxLayout.X_AXIS));
		pol12.add(enter);
		
		panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol2);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol5);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol6);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol12);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    
	    frame.add(panel);
	    frame.setVisible(true);
		enter.addActionListener(new creareComanda());
	
		
		
	}

	private void setCombo(Collection<MenuItem> meniu) throws ClassNotFoundException, SQLException {
		
		for(MenuItem m:meniu) {
		tNumeProdus.addItem(m.getNume());
			}
		}

	class creareComanda implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				
				String dataCurenta;
				GregorianCalendar gcalendar = new GregorianCalendar();
				gcalendar.set(gcalendar.get(Calendar.YEAR), gcalendar.get(Calendar.MONTH), gcalendar.get(Calendar.DATE) );
				dataCurenta= String.format("%d.%d.%d",gcalendar.get(Calendar.DATE),gcalendar.get(Calendar.MONTH)+1,gcalendar.get(Calendar.YEAR));
				o=new Order(Integer.parseInt(tId.getText()),dataCurenta,1+tMasa.getSelectedIndex());
				
				
				String n=new String();
				n=(String) tNumeProdus.getSelectedItem();
				BaseProduct b=null;
				int pret=0;
				for(MenuItem j:r.getMeniu()) {
					if(j.getNume().equals(n)) //pret=Integer.parseInt(j.getPret());
						pret=j.computePrice();
				}
				b= new BaseProduct(pret,n);
				
				m=b;
				int ok=0;
				
				for(Order j:r.getO())	
				{
					
					if(j.equals(o))
					{	ok=1;
					Collection<MenuItem> v=r.getComanda().get(o);
					v.add(m);
					r.getO().add(o);
					r.computePrice(o);
				}
				if(ok==0) {					

					o.setPret(m.computePrice());
					ArrayList<MenuItem> p=new ArrayList<MenuItem>();
					p.add(m);
					r.computePrice(o);
					r.getComanda().put(o, p);
				
				}
			}} 
				catch (Exception e1) {
				e1.printStackTrace();
			}chef.update(null,null);
		
		}
	}

	public Order getO() {
		return o;
	}

	public MenuItem getM() {
		return m;
	}

	public ArrayList<MenuItem> getProduse() {
		produse.add(m);
		return produse;
	}

}