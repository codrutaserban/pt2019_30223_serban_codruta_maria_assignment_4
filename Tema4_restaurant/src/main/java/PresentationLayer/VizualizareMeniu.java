package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;


public class VizualizareMeniu {
	private JTable tabel=new JTable();
	private DefaultTableModel model = new DefaultTableModel();
	private Object[] capDeTabel = new Object[2];
	private Object[] date = new Object[2];
	private Restaurant r;
	private JFrame frame = new JFrame("Vizualizare meniu"); 
	
	public VizualizareMeniu (Restaurant r) throws ClassNotFoundException, SQLException{
	this.r=r;
	JScrollPane scroll;
	JPanel panel=new JPanel();
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	frame.setSize(500, 480);
	capDeTabel[0]="Nume";
	capDeTabel[1]="Pret";
	model.setColumnIdentifiers(capDeTabel);
	try {
		
		for(MenuItem i:r.getMeniu()) {
			date[0]=i.getNume();
			date[1]=i.getPret();
			model.addRow(date);
		}
	} catch (Exception e) {}
	
	
	tabel.setModel(model);
	tabel.setFont(new Font("Arial",Font.PLAIN,12));
	tabel.setEnabled(false);
	scroll= new JScrollPane(tabel);
	panel.add(scroll,new FlowLayout());
	frame.add(panel);
	frame.setVisible(true);
	
	}
	public void setR(Restaurant r) {
		this.r = r;
	}
}
