package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import BusinessLayer.BaseProduct;
import BusinessLayer.Restaurant;


public class PaginaPornire {

	private JFrame frame;
	private JTextArea mesaj= new JTextArea("     Alegeti o optiune:");
	private JButton admin= new JButton("Administrator");
	private JButton chelner= new JButton("Chelner");
	private JButton chef= new JButton("Chef");
	private InterfataChef c=new InterfataChef();
	private Restaurant r;
	
	public PaginaPornire( Restaurant r){
		this.r=r;
		mesaj.setEditable(false);
		
		frame = new JFrame("Prima pagina");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(350, 325);
		mesaj.setFont(new Font("Arial",Font.PLAIN,25));
		admin.setFont(new Font("Arial",Font.PLAIN,25));
		chelner.setFont(new Font("Arial",Font.PLAIN,25));
		chef.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(mesaj);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(chelner);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(admin);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
		panel.add(chef);
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    frame.add(panel);
		frame.setVisible(true);
		admin.addActionListener(new opAdmin());
		chelner.addActionListener(new opChelner());
		chef.addActionListener(new opChef());
	}
	private class opAdmin implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataAdmin admin=new InterfataAdmin();
				admin.setR(r);
		}
	}
	
	private class opChelner implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataChelner chelner=new InterfataChelner();
				chelner.setR(r);
		}
	}
	
	private class opChef implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataChef chef=new InterfataChef(r);
		}
	}
}
