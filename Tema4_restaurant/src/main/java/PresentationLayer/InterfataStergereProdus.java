package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BusinessLayer.Restaurant;


public class InterfataStergereProdus {
	private JFrame frame = new JFrame("Stergere produs");
	private JLabel info= new JLabel("Introduceti numele produsului:");
	private JTextField tIdProdus= new JTextField("");
	private JButton enter= new JButton("Enter");
	private String s;
	private Restaurant r;
	
	InterfataStergereProdus(Restaurant r){
		this.r=r;
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(550, 275);
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    info.setFont(new Font("Arial",Font.PLAIN,25));
	    tIdProdus.setFont(new Font("Arial",Font.PLAIN,25));
	    enter.setFont(new Font("Arial",Font.PLAIN,25));
	    
	    JPanel p= new JPanel();
	    p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
	    p.add( Box.createRigidArea(new Dimension(20,00)) );
	    p.add(info);
	    
	    JPanel p1= new JPanel();
	    p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
	    p1.add( Box.createRigidArea(new Dimension(200,00)) );
	    p1.add(tIdProdus);
	    p1.add( Box.createRigidArea(new Dimension(180,00)) );
	    
	    JPanel p2= new JPanel();
	    p2.setLayout(new BoxLayout(p2, BoxLayout.X_AXIS));
	    p2.add( Box.createRigidArea(new Dimension(20,00)) );
	    p2.add(enter);
	    
	    
	    panel.add( Box.createRigidArea(new Dimension(0,50)) );
	    panel.add(p);
	    panel.add( Box.createRigidArea(new Dimension(0,20) ));
	    panel.add(p1);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(p2);
	    panel.add( Box.createRigidArea(new Dimension(0,300)) );
		frame.add(panel);
		frame.setVisible(true);
		enter.addActionListener(new pStergere());
	}
	private class pStergere implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				s= tIdProdus.getText();
				r.stegereProdusMeniu(s);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	public String getS() {
		return s;
	}
}
