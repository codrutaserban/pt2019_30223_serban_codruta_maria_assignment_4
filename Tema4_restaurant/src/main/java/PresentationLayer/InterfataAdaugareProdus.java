package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BusinessLayer.BaseProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;


public class InterfataAdaugareProdus {

	private JFrame frame = new JFrame("Adaugare produs");
	private JLabel info= new JLabel("Completati urmatoarele campuri:");
	private JLabel nume= new JLabel("Nume: ");
	private JLabel pret= new JLabel("Pret: ");

	
	private JTextField tNume= new JTextField("");
	private JTextField tPret= new JTextField("");
	
	private JButton enter= new JButton("Enter");
	private MenuItem m;Restaurant r;
	

	InterfataAdaugareProdus(Restaurant r){
		this.r=r;
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(550, 300);
		enter.setFont(new Font("Arial",Font.PLAIN,25));
		info.setFont(new Font("Arial",Font.PLAIN,25));
		nume.setFont(new Font("Arial",Font.PLAIN,25));
		pret.setFont(new Font("Arial",Font.PLAIN,25));
		
		
		tNume.setFont(new Font("Arial",Font.PLAIN,25));
		tPret.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    
	    JPanel pol1 = new JPanel();
	    pol1.setLayout(new BoxLayout(pol1, BoxLayout.X_AXIS));
	    pol1.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol1.add(nume);
	    pol1.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol1.add( tNume);
	    pol1.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    
	
	    
	    JPanel pol3 = new JPanel();
	    pol3.setLayout(new BoxLayout(pol3, BoxLayout.X_AXIS));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol3.add(pret);
	    pol3.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol3.add(tPret);
	    pol3.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	  
	    
	    JPanel pol= new JPanel();
	    pol.setLayout(new BoxLayout(pol, BoxLayout.X_AXIS));
	    pol.add(info);
	    
	    JPanel pol12= new JPanel();
	    pol12.setLayout(new BoxLayout(pol12, BoxLayout.X_AXIS));
	    pol12.add(enter);
	    
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol1);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol3);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol12);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    frame.add(panel);
	    frame.setVisible(true);
	   enter.addActionListener(new pAdaugare());
	}
	private class pAdaugare implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				BaseProduct p= new BaseProduct(Integer.parseInt(tPret.getText()),tNume.getText());
				m=p;
				r.adaugareProdusMeniu(p);
				r.getMeniu().add(p);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	public MenuItem getM() {
		return m;
	}

}
