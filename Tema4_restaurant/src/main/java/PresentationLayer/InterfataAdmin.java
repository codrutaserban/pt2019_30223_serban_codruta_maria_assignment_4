package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.text.html.HTMLDocument.Iterator;

import BusinessLayer.Restaurant;




public class InterfataAdmin {
	private JButton adaugare= new JButton("Adaugare produs in meniu");
	private JButton editare=new JButton("Editare produs din meniu");
	private JButton stergere= new JButton("Stergere produs din meniu");
	private JButton vizualizare=new JButton("Vizualizare meniu");
	private JFrame frame;
	InterfataStergereProdus produsSt;
	Restaurant r;
	
	public InterfataAdmin (){
		frame = new JFrame("Produs");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(450, 325);
		
		adaugare.setFont(new Font("Arial",Font.PLAIN,25));
		editare.setFont(new Font("Arial",Font.PLAIN,25));
		stergere.setFont(new Font("Arial",Font.PLAIN,25));
		vizualizare.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    panel.add( Box.createRigidArea(new Dimension(150,25)) );
	    panel.add(adaugare);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(editare);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(stergere);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
		panel.add(vizualizare);
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    frame.add(panel);
	    frame.setVisible(true);
	    adaugare.addActionListener(new opAdaugare());
	 	editare.addActionListener(new opEditare());
	 	stergere.addActionListener(new opStergere());
	 	vizualizare.addActionListener(new opViz());
}
	private class opAdaugare implements ActionListener {
	 		public void actionPerformed(ActionEvent e){
	 				InterfataAdaugareProdus produsAd=new InterfataAdaugareProdus(r);
	 				
	 		}
	 	}
		
	 	private class opEditare implements ActionListener {
	 		public void actionPerformed(ActionEvent e){
	 				InterfataProdusEditare produsEd=new InterfataProdusEditare(r);
	 		}
	 	}
	 
	 	private class opStergere implements ActionListener {
	 		public void actionPerformed(ActionEvent e){
	 	 produsSt=new InterfataStergereProdus(r);
	 		}
	 	}
	 	private class opViz implements ActionListener {
	 		public void actionPerformed(ActionEvent e){
	 			try {
					VizualizareMeniu produsViz=new VizualizareMeniu(r);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	 		}
	 	}
		public void setR(Restaurant r) {
			this.r = r;
		}
}
