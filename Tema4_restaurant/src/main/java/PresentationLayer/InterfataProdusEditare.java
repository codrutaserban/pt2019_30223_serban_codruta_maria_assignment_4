package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;


public class InterfataProdusEditare {
	private JFrame frame = new JFrame("Editare produs");
	private JLabel info= new JLabel("Completati urmatoarele informatii:");
	private JComboBox cb= new JComboBox(new String[]{"","Nume", "Pret"});
	private JLabel camp= new JLabel("Camp: ");
	private JLabel numeProdus= new JLabel("Numele produsului: ");
	private JLabel val= new JLabel("Noua valoare: ");
	private JButton enter= new JButton("Enter");
	
	private JTextField tNumeProdus= new JTextField("");
	private JTextField tVal= new JTextField("");
	
	
	private String s;
	private String nume;
	private String nouaVal;
	private Restaurant r;
	
	InterfataProdusEditare(Restaurant r){
		this.r=r;
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(550, 480);
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
	    info.setFont(new Font("Arial",Font.PLAIN,25));
	    camp.setFont(new Font("Arial",Font.PLAIN,25));
	    cb.setFont(new Font("Arial",Font.PLAIN,25));
	    numeProdus.setFont(new Font("Arial",Font.PLAIN,25));
	    val.setFont(new Font("Arial",Font.PLAIN,25));
	    tNumeProdus.setFont(new Font("Arial",Font.PLAIN,25));
	    tVal.setFont(new Font("Arial",Font.PLAIN,25));
	    enter.setFont(new Font("Arial",Font.PLAIN,25));
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(info);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(numeProdus);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	   panel.add(tNumeProdus);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(camp);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(cb);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(val);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(tVal);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(enter);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		frame.add(panel);
		frame.setVisible(true);
		enter.addActionListener(new pEditare());
	}
	private class pEditare implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				s= (String) cb.getSelectedItem();
				nume= tNumeProdus.getText();
				nouaVal=tVal.getText();
				
				
 				for(MenuItem i:r.getMeniu()) {
 					if(i.getNume().equals(nume))
 						{r.editProdusMeniu(i,s,nouaVal);break;}
			
			}} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		}
	public String getCamp() {
		return s;
	}
	
	public String getNume() {
		return nume;
	}
	
	public String getNouaVal() {
		return nouaVal;
	}
	
	
}
